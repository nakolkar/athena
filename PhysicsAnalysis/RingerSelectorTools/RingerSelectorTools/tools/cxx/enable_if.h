/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// $Id: enable_if.h 693383 2015-09-06 22:08:55Z wsfreund $
#ifndef RINGERSELECTORTOOLS_TOOLS_CXX_ENABLE_IF_H
#define RINGERSELECTORTOOLS_TOOLS_CXX_ENABLE_IF_H

#include "RingerUseNewCppFeatures.h"


#include<type_traits>
namespace Ringer {
using std::enable_if;
} // namespace Ringer


#endif // RINGERSELECTORTOOLS_TOOLS_CXX_ENABLE_IF_H


