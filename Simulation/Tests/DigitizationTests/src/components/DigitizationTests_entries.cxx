#include "DigitizationTests/DigiTestAlg.h"
//#include "DigitizationTests/PileUpEventInfoTest.h"
#include "../McEventCollectionTestTool.h"
#include "../PileUpEventInfoTestTool.h"
#include "../PixelRDOsTestTool.h"
#include "../SCT_RDOsTestTool.h"
#include "../TRT_RDOsTestTool.h"


DECLARE_COMPONENT( McEventCollectionTestTool )
DECLARE_COMPONENT( PileUpEventInfoTestTool )
DECLARE_COMPONENT( PixelRDOsTestTool )
DECLARE_COMPONENT( SCT_RDOsTestTool )
DECLARE_COMPONENT( TRT_RDOsTestTool )
DECLARE_COMPONENT( DigiTestAlg )
//DECLARE_COMPONENT( PileUpEventInfoTest )

