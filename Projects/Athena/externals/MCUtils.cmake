#
# File specifying the location of MCUtils to use.
#

set( MCUTILS_LCGVERSION 1.3.4 )
set( MCUTILS_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/mcutils/${MCUTILS_LCGVERSION}/${LCG_PLATFORM} )
