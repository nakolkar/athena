#
# File specifying the location of Crmc to use.
#

set( CRMC_LCGVERSION 1.6.0 )
set( CRMC_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/crmc/${CRMC_LCGVERSION}/${LCG_PLATFORM} )
